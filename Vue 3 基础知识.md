# Vue知识点回顾:

[官网地址](http://cn.vuejs.org)  http://cn.vuejs.org

## 安装 nodejs 环境 

> 为了安装一个 npm 包管理器，用来管理前端项目需要的依赖库 

## 创建一个 Vue 工程 

- 基于 webpack 管理项目

```
vue create <projectname>
```

- 基于 vite 管理项目

```
npm init vue@latest

```

## 构建一个 Vue2 实例对象 

```
import Vue from 'vue' ;

new Vue({
   el: "#app", 
   data: {
      // 定义响应式数据
   },
   methods: {
      // 定义方法函数
   },
   components: {
      // 注册组件
   },
   computed: {
      // 定义计算属性
   },
   filters: {
      // 定义过滤器 
   },
   watch : {
      // 监控响应式数据
   },
   created() {
      // 响应式数据加载完成后触发的代码
   },
   mounted() {
      // 虚拟dom 加载完成 触发的代码
   }
})

```

## 构建一个 Vue3 实例对象 

```
import {createApp} from 'vue'


createApp({

   data() {
      return {
        // 定义响应式数据
      }
   },
   methods: {
   
   },
   computed: {
   
   },
   filters: {
   
   },
   components: {
   
   },
   watch: {
   
   },
   
   created() {
   
   },
   
   mounted() {
   
   }

}).mount("#app");


```

## 组件式文件 .vue 

> 一个 .vue 文件 由三部分组成 
template 标签 用来定义 html 片段 
style 标签 用来定义 css 样式 
script 标签 用来定义 JS 代码 

### template 标签 

> 该标签是一个不可见标签，主要应用在 vue 框架中，用来包裹 html元素 
vue 2 中 template 标签下 有且只有一个 根标签
vue 3 中 template 标签下 对根标签的个数没有要求，但建议 一个 

### vue 中常见的指令 

> vue中的指令以 `v-` 开头，且只能必须作为 标签的属性(依赖于标签)出现

- {{ key }} :  在模板的 text 文件节点中 输出 响应式数据 key 对应的值， key 支持 javascript 表达式
- v-text : 给标签体插入文本内容
- v-html : 给标签体插入 超文本内容

> 在 vue2 中 如果一个 标签使用了 v-text/ v-html设置标签体内容，且标签体中本身也有内容
那么 标签体中的内容 会被 v-text / v-html 得到的结果 进行覆盖
在 vue3 中 如果一个标签使用了 v-text / v-html ，那么该标签的标签体中 不允许设置子元素

- v-bind : 给标签的属性绑定数据 
  - v-bind:prop=val : 给指定的属性绑定对应的值 
  - v-bind:[prop]=val : 将 prop 表达式运算的结果作为要绑定的属性名
  - v-bind=obj : 批量将 obj 对象中的属性 依次 绑定，作为标签中的属性和值
- v-bind:style : 绑定 行内样式 
  - obj : 将一个对象作为绑定的值  {color: '#f00'}
  - array: 将一个数组(存放多个对象)作为绑定的值 [{color: '#f00'}, {fontSize: '20px'}]
  - obj : 将一个对象作为绑定的值，属性对应的值是一个array, {display: ['-webkit', 'flex']}
- v-bind:class : 绑定类样式 
  - {className: bool, className2: bool} : 键是要添加的样式名，值决定要不要添加该样式
  - [className, className2, ...]  : 绑定多个样式
  - [className,  {className2: bool} ] :
- v-if : 判断 ，完整结构是 v-if / v-else-if / v-else 
- v-show : 显示 ，本质是通过 css 中 display 属性 控制标签的显示/隐藏，v-show不能作用到 template标签上
- v-for ：循环 
  - 数字遍历  v-for="n in 10" : 从 1 ~ 10 的遍历，数字遍历从 1 开始
  - 数组遍历  v-for="(item, index) in array"  
  - 对象遍历  v-for="(val, key, index) in obj"

> 在使用 v-for 进行遍历数据的过程中，推荐绑定一个 key。 Vue 采用`就地更新`的策略, 当对遍历的数据
进行 添加、删除的时候，如果没有指定 key ,那会可能会产生数据紊乱，建议在使用 v-for的时候，绑定 key。 
key 在整个 for 循环中，保证它的值唯一。
v-for也可以作用在 template 标签上 

**v-for 中如果使用 v-if ,那么建议 不要把 v-for 和 v-if 写在同一个标签上、因为他们的优先级不确定**
**vue 2中 v-for的优先级大于v-if, vue3中 v-if的优先级大于v-for**

- v-on : 绑定事件，可以简写为 `@`
  - v-on:事件名="内联JS脚本" : 通常适用于相对比较简单的脚本
  - v-on:事件名="函数" :  可以处理较为复杂的逻辑 
  - v-on:事件名="函数(...)" : 在绑定函数的时候，可以格外的传递参数
    - 键盘事件 keydown, keyup, keypress
    - 常用的键盘修饰符  .esc ,  .enter ,  .tab
    - 常见的通用修饰符  .prevent (阻止默认行为) ,  .stop (阻止事件冒泡) ，

>  v-on:事件名="函数" , 函数需要定义在 methods 选项中，且该函数 可以接收一个 event 参数，代表触发事件的 事件对象 
 v-on:事件名="函数(...)" , 函数仍需要定义在methods选项中，该函数接收的参数由 (...) 中传入的内容决定，如果要使用
事件对象 event , 那么 可以在绑定事件的时候，将 $event 作为函数的参数 传入 即可  

- v-model :

> v-model 主要应用与 表单 和 组件上， v-model 只能传入 响应式数据、不支持 JS 表达式。
v-model 其实式一个语法糖(简写)、本质上是  v-bind 和 v-on 的简写 

## 安装 VUE 插件  ==vuejs devtools==

## 组件 Component 

> 构成一个完整页面的 部分片段代码，通过组件 可以将一个 大的页面 拆分成若干个小的页面。
组件具有可复用性 ，可以将页面中 相同或者相似的结构 进行组件化。可以大大的提高复用率。
在宏观上 ，一个组件 就是对应一个 .vue 模板文件 

### 组件应用的四大步骤 

1. 声明一个组件 (创建一个 .vue 模板文件、组件推荐放到 components 文件夹下)
2. 在使用的位置 导入 需要的组件

```
import 组件名 from 组件位置 
```

3. 注册组件

```javascript
export default {

   ... 
   components: {
       ..注册组件 
   }
}
```

4. 在模板中使用 组件

<br/>

### 组件的通信

> 组件与组件之间的关系 主要分为   父子组件 、  子父组件 、 兄弟组件 、其他关系组件

#### 父组件向子组件传参

- 在 使用 子组件的时候 ， 通过 组件标签的自定义属性的 方式 将 父组件中的数据 传递 给 子组件

```javascript
<Star  :star="star" />

export  default {
      data(){
            return {
                    star: 1 ,
            }
    }
}
```

- 在 子组件中 ，通过 props 选项  接受 父组件传递的数据 
  - props  数组格式:  
    ```
    props: ["star"] 
    ```
  - props  对象格式
    - 接受的参数  指定 数据类型 
      ```
      props:  {
          star:  Number 
      }
      ```
    - 接受的参数是 对象格式
      ```
      props: {
          star: {
              type:  [Number , String] ,    // 该属性也可以是个数组，代表 值得类型 有多种 
              required: false  ,   // 该属性是否 要求 父组件 必传， 默认是 false ( vue2 中 默认值 是 true)
              default:  1   ,   // 如果 父组件没有传递，则 取默认值 
              validator: function(val) { // return bool }  // 校验值的格式 
          }
      }
      ```

>  **父组件向子组件传递的数据、在子组件中是只读的**,  父组件传递的数据采用单向数据流、当父组件中的数据发生改变，会自动流向子组件、子组件可以定义一个自己的响应式数据、将父组件第一次传入的数据 默认作为 初始值 。 如果 希望 父组件传递的数据 发生改变，子组件 也发生改变，可以采用 watch 监控 父组件传递的数据。

<br/>

####  子组件向父组件传递数据 

- 在 子组件中 ， 通过 触发事件  ，然后  通过 ` $emit `函数  向 指定的 `频道(自定义事件)` 发送 消息 
  ```javascript
  export default {
     ...
    emits: ["update:star"] ,
    methods: {
      handlerStar(n) {
        this.value = n ;
        // 通知 父组件 ，子组件 修改了 数据
        this.$emit("update:star", n);
      }
    }
  }
  ```

> update:star  自定义的 频道 (自定义事件) ,    n  是 载荷 (附带的数据)

- 在 父组件中 ， 在 使用的 组件上 通过 事件 绑定的 方式  来 监听 频道 发送的消息 
  ```javascript
  <Star  :star="star"  @update:star="updateStar" />
  
  export  default {
    data(){
        return {
           star: 1 ,
        }
      },
      methods: {
        updateStar(n) {
            this.star = n ;
        }
      }
  }
  ```

> 当父组件 通过 监听 `频道` 方式 获取 子组件传递的数据的时候，可以通过 内置对象 `$event`  获取 子组件传递的数据 

#####  v-model  在 组件上的应用 

-  vue 2 中 v-model 的特点

> v-model 如果 用在组件上、默认绑定的是  value 属性  和 input 事件 

```
<Star  v-model="star" />

等价于 

<Star  :value="star"  @input="star = $event" />

```

> 如果 要更改 v-model 绑定的 属性 和 事件名， 可以在 子组件中 通过 model 选项进行修改 

```javascript
export default  {
   
   ...
   model: {
    prop:  "value" , 
    event: "input"
   }
}
```

- vue 3 中 v-model 的特点 

> v-model  如果使用在 组件上、默认绑定的 是 modelValue 属性 和 update:model-value 事件 ，绑定的值不能修改
> 
> ```
> <Star  v-model="star" />
> 
> 等价于 
> 
> <Star  :modelValue="star"  @update:model-value="star = $event" />
> 
> ```

###  在 父组件 中 直接 子组件的数据 

>  在 使用 子组件的时候 ， 给 子组件 添加 一个  `ref` 属性 ,  在 父组件中， 可以 通过  `$refs`  内置对象 获取 指定的 ref 对应的组件 ，从而获取组件中定义的数据 

### 获取 根组件的数据

>  在任意组件中， 可以通过 $root 获取根组件实例，从而操作 根组件中的数据 

<br/>

### 插槽 slot 

> 对组件的一种扩展、体现了组件的可扩展性 ~ 

####  定义一个插槽 

```javascript
<slot></slot>
<slot name="icon"></slot>
```

>  如果一个插槽 没有名字，那么默认名 为 default  ,  可以 通过 name 属性 设置 插槽的名字

#### 后备插槽 

>  可以在定义插槽的时候，设置 插槽的默认值 ，如果 在使用组件的时候，没有设置该插槽，那么默认值会生效
> 
> ```javascript
> <slot><button>搜索</button></slot>
> ```

#### 作用域插槽 

>  将 子组件中的 需要 暴露 给 插槽的数据 ，通过 在 插槽上 进行 数据绑定（属性绑定）的方式 传递给 父组件 分发插槽的地方 

```javascript
<slot name="icon" :search="text"></slot>

export default {
  
   data() {
     
      return {
        
        text: "xxx",
      }
   }
  
}
```

#### 使用插槽 

```javascript
  <Search>
    <template v-slot:btn>
       <button>搜索</button>
    </template>

    <template #icon="{search}">
      <button>图标</button>
    </template>

    <a>百度</a>
  </Search>
```

> 通过 v-slot 指定要分发的内容， v-slot 要写在 template 标签上 ，  v-slot:name  可以简写为  #name。
> 
> 如果 没有指定分发的位置，那么 会 将内容 默认分发到 default 插槽中，如果没有声明 对应的插槽，那么 分发的内容会被丢弃

## 路由 router 

> 通过在地址栏中输入 一个 网络地址(请求地址) 、根据 该地址 找到 和 vue 组件的 对应关系 ，从而 实现 根据 请求地址 加载 对应的  Vue 、实现 页面展示效果 。
> 
> 实现的是  请求 地址  和  视图页面 的映射关系 

<br/>

### 在 Vue 2 中使用 路由的步骤 

1.   安装 vue-router 
   ```javascript
   npm  install  vue-router
   ```

2.  在 src/ 目录下 新建一个 router 文件夹、并创建一个 index.js  文件 
3.  在 index.js 文件声明 路由 
   ```javascript
   const  routes = [
       {
           name: 'login', 
           path:  '/login', 
           component: ....
       },
       ...
   ]
   
   
   const router =  new VueRouter({
       routes , 
   })
   
   
   export default  router ;
   ```

4.  将 router 挂载到 vue 实例对象上 (main.js)
   ```javascript
   import router from './router'
   
   new  Vue({
   
       ... 
       router ,
   }).$mounted("#app");
   ```

### 在 Vue3 中使用 路由的步骤  

1.   安装  vue -router 库
   ```javascript
   npm  install  vue-router
   ```

2.  在 src/ 目录下 新建一个 router 文件夹、并创建一个 index.js  文件
3.  在 index.js 文件声明 路由
   ```javascript
   import  {createRouter} from 'vue-router'
   
   
   const routes = [
       {
         name : 'login' , 
         path: '/login' , 
         component: ....
       }
   ]
   
   
   const router = createRouter({
       routes ,
   })
   
   export default router ;
   ```

4.  在 main.js  挂载  router 
   ```javascript
   import router from './router'
   ...
   const  app  = createApp(App);
   ...
   app.use(router);
   
   app.mount("#app");
   ```

### 动态路由 

> 在定义路由的时候，路由的地址 允许使用` :key`  进行 占位 ，标记该位置需要传递一个动态数据
> 
> 在  vue 2 中，使用  $route 获取 当前路由对象，  vue2  API模式下仍旧可以使用， 也可以使用  Vue3 官方推荐的写法 。
> 
> import  { useRoute }  from  'vue-route' 
> 
> let  route  = useRoute();

#### $route 当前路由对象 

-  fullPath :  获取当前 路由的 完成路径 ，包含 ? 分割的参数 
- path : 获取 当前路由的  路径， 不包含 ？分割的参数 
- params :  获取 动态 路由  对应的 参数 ，是一个 对象
- query :  获取 路由的 查询 参数 (? 分割的参数)
- meta :  路由的元数据 
- hash :  路由的 锚点 （hash）

<br/>

#### $router  路由管理对象 

-  push (obj)  :   跳转到 指定的 路由中 
  ```
  $router.push({name: "index"})   //  跳转到 路由名称为 index 的 路由中 
  
  $router.push({path: "/"})   //  跳转到 path 指定的 路由地址 中 
  
  $router.push("/")   // 跳转到 / 路由地址中 
  
  $router.push({name: "detail" ,  params: {id: 2}}) //  跳转到 路由名称为 detail 的动态路由中， 且给 占位符 id 传入的值 为 2 
  
  $router.push({name: "index",  query: {a: 1,  b: 2}}) // 跳转到 路由名为 index的 路由中，且传了2个查询参数 分别是 a 和 b 
  
  ```

- replace (obj) ：跳转到指定的路由中， 用法和 push 完全相同 

>  push 和 replace  都可以实现 路由的跳转，  push  在跳转的时候，会将 跳转信息 记录 到 历史 记录 history 中，可以实现 浏览器的 前进/ 后退功能
> 
> replace是 直接替换 当前路由地址， 不会将 信息写入到 histroy 中 

- go(n)  :  实现 前进/后退  n  步 ，  n > 0 代表前进 ， <0 代表后退， 通过 用  1  和 -1
- forward() :  前进 1步 
- back()  : 后退 1 步

<br/>

### 路由模式 

-  Vue 2 中的 路由模式 
  ```javascript
  new  VueRouter({
  
      routes , 
      
      mode:  "history" | "hash" 
      
  })
  ```

- Vue 3 的 路由模式 
  ```javascript
  import { createWebHistory  ,  createWebHashHistory   } from 'vue-router'
  
  new  createVueRouter({
  
      routes , 
      
      history :  createWebHistory() |   createWebHashHistory() 
  })
  ```
