# 搭建环境 

## web.xml 配置

- 配置监听器
  - @Controller   ：  在控制器类上编写的 注解，用来管理 控制器对象
  - @Service        ：  在业务层类上编写的 注解，用来管理业务层对象
  - @Repository  :    在持久层类上编写的 注解，用来管理持久层对象
  -  @Component  ： 在 除 上述 三层 之外的 其他 层 使用的注解， 用来管理 对象， 通用注解
  - @Resource   :    用来 注入 对象 
  - @Transactional :   启用事务管理 ， 通常用在业务逻辑层 类上

```xml
<!-- 用来初始化 ApplicationContext,  让项目支持 @Controller,   @Service , @Repository,  @Component ，  @Resource  注解  -->

<listener>
    <listener-class>org.haredot.listener.ClassLoaderListener</listener-class>
</listener>
```

-  配置 核心 控制器 
  >  核心 控制器 用来 完成 请求的分发 、跳转 、参数注入 等功能 ，支持的注解有  @RequestMapping ,    @RequestParam ,  @RequestPart,   @RequestBody ,   @ResponseBody
  -  @RequestMapping  :   用来 映射请求 地址 和 请求方式， 默认请求方式为 GET, POST 
  - @RequestParam  :  用来接收 表单 类型的参数 (form表单参数 和  查询请求参数) 
  - @RequestPart  :   用来 接收 上传的文件对象， 注释的参数类型必须 是  Part 
  - @RequestBody  :  用来 接受 JSON 格式的 参数 
  - @ResponseBody  :  用来 标记 控制器 返回 JSON 格式的数据 
  ```xml
  <servlet>
      <servlet-name>servlet</servlet-name>
      <servlet-class>org.haredot.web.dispatcher.DispatcherServlet</servlet-class>
      <!-- 随 tomcat 启动初始化 init 方法，  init 方法 完成了 Web容器的加载  -->
      <load-on-startup>1</load-on-startup>
      <!-- 支持 文件上传 -->
      <multipart-config></multipart-config>
  </servlet>
  
  <servlet-mapping>
      <servlet-name>servlet</servlet-name>
      <url-pattern>/</url-pattern>
  </servlet-mapping>
  ```
  ```
  
  ```

- 配置 字符集编码 过滤器 
  >  解决  请求 中文乱码问题 
  ```xml
  <filter>
      <filter-name>encodingFilter</filter-name>
      <filter-class>org.haredot.filter.CharacterEncodingFilter</filter-class>
      <init-param>
          <param-name>encoding</param-name>
          <param-value>UTF-8</param-value>
      </init-param>
  </filter>
  <filter-mapping>
      <filter-name>encodingFilter</filter-name>
      <url-pattern>/*</url-pattern>
  </filter-mapping>
  ```

- 配置  Form 表单内容过滤器 
  >  解决 PUT ,  PATCH , DELETE  不支持 表单传参问题
  ```xml
  <filter>
      <filter-name>formContentFilter</filter-name>
      <filter-class>org.haredot.filter.FormContentFilter</filter-class>
  </filter>
  <filter-mapping>
      <filter-name>formContentFilter</filter-name>
      <url-pattern>/*</url-pattern>
  </filter-mapping>
  ```

- 配置 默认Servlet处理器
  > 解决静态资源无法访问问题
  ```xml
  <servlet-mapping>
      <servlet-name>default</servlet-name>
      <url-pattern>/static/*</url-pattern>
  </servlet-mapping>
  ```

##  转发到 JSP 页面

```java
@Controller
public  class  UserController {
    
    @RequestMapping("/user")
    public  ModelAndView  user() {
      
        return  new ModelAndView("user") ;
    }
}
```

>  ModelAndView  传入的 视图名 为  user , 则 默认转发到  /WEB-INF/pages/user.jsp ,   前缀默认为 /WEB-INF/pages/ ,   后缀默认为  .jsp  ,  可以在 配置 核心控制器的时候，进行修改
> 
> ```xml
> <servlet>
>     <servlet-name>servlet</servlet-name>
>     <servlet-class>org.haredot.web.dispatcher.DispatcherServlet</servlet-class>
>     <!-- 随 tomcat 启动初始化 init 方法，  init 方法 完成了 Web容器的加载  -->
>     <load-on-startup>1</load-on-startup>
>     <!-- 支持 文件上传 -->
>     <multipart-config></multipart-config>
>     <init-param>
>         <param-name>prefix</param-name>
>         <!--修改为 /WEB-INF/jsp/ -->
>         <param-value>/WEB-INF/jsp/</param-value>
>     </init-param>
>     <init-param>
>         <param-name>suffix</param-name>
>         <param-value>.jsp</param-value>
>     </init-param>
> </servlet>
> 
> <servlet-mapping>
>     <servlet-name>servlet</servlet-name>
>     <url-pattern>/</url-pattern>
> </servlet-mapping>
> ```

##  转发到 某个请求 

```java
@Controller
public  class  UserController {
    
    @RequestMapping("/user")
    public  ModelAndView  user() {
      
        return  new ModelAndView("forward:/login") ;
    }
}
```

##  重定向到某个请求

```java
@Controller
public  class  UserController {
    
    @RequestMapping("/user")
    public  ModelAndView  user() {
      
        return  new ModelAndView("redirect:/login") ;
    }
}
```

##  接收表单参数

```java
@Controller
public  class  UserController {
    
    @RequestMapping("/user")
    public  ModelAndView  user(@RequestParam("id") Long id ,  @RequestParam(value="sex",  required=false, defaultValue="m") String sex) {
      
        ...
    }
}
```

-  RequestParam 中常见的 成员
  - value :  必传，  用来 设置 请求参数对应的  key  
  - required :  默认为 true ,  是否要求改参数必传，  如果没有传，则会报错
  - defaultValue :  配合 required = false 使用， 当参数不传的时候，设置的默认值 

##  接收 文件上传对象

```java
@Controller
public  class  UserController {
    
    @RequestMapping(value="/user", method= RequestMethod.POST)
    public  ModelAndView  user(@RequestPart("photo") Part part) {
      
        ...
    }
}
```

##  接收 JSON 请求参数

```java
@Controller
public  class  UserController {
    
    @RequestMapping(value="/user", method= RequestMethod.PUT)
    public  ModelAndView  user(@RequestBody User user) {
      
        ...
    }
}
```

## 获取 HttpServletRequest 等对象

```java
@Controller
public  class  UserController {
    
    @RequestMapping(value="/user", method= RequestMethod.PUT)
    public  ModelAndView  user(HttpServletRequest request,  HttpServletResponse response,  HttpSession session,  ServletContext  application) {
        // 上述 四个对象 均可以直接注入到 控制器 对应的方法中，直接使用  
        ...
    }
}
```

> 也可以使用 工具类  WebUtils 中提供的对应方法 获取 

## 返回 JSON

```java
@Controller
public  class  UserController {
    
    @RequestMapping("/user")
    @ResponseBody
    public  User user(@RequestBody User user) {
      
        return user ;
    }
}
```

>  @ResponseBody 标记的方法 可以将 返回值 （必须是可以转换为JSON的对象，不能是ModelAndView） 转换为 JSON

## 文件下载

```java
@Controller
public  class  UserController {
    
    @RequestMapping("/user")
    public  ResponseEntity user() {
        // 获取要下载的 流数据
        byte[]  data = ... ; 
        // 要下载的文件名
        String filename = "a.jpg" ;
        
        return ResponseEntity.builder()
                .contentType("image/jpeg")   // 设置文件的媒体类型， 如果做附件下载，也可以省略该设置
                .attachment(true)   // 是否采用附件进行下载， 默认预览（如果要下载的资源不支持预览，则自动下载）
                .filename(filename)  // 设置下载的文件名， 支持中文
                .body(data) ;   // 设置要下载的数据，该属性必须最后调用
    }
}
```
