#  Eureka 

>   能够实现 服务发现与注册、 实现服务治理。  常见的 服务治理 组件 有   Eureka (netfilix) 、  Confsul  (SpringCloud 推出) 、Nacos (阿里巴巴) 、 Zookpeer 

<br/>

##  Eureka Server  (Eureka服务)

>  实现 服务发现、  能够 自动管理所有的 Eureka Client  ,  所以 只需要让 所有 需要 交给 Eureka 管理的 微服务  变成  Eureka Client  即可 ！

- 添加 Eureka Server 依赖包
  ```xml
  <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
  </dependency>
  ```

- 在 启动类  /  配置类  添加 @EnableEurekaServer  
  ```java
  @EnableEurekaServer
  @SpringBootApplication
  public class EurekaServerApplication {
  
      public static void main(String[] args) {
          SpringApplication.run(EurekaServerApplication.class, args);
      }
  }
  
  ```

- 编写 application.yml  ,  配置 eureka 注册地址
  ```yaml
  server:
    port: 8761
  
  # 配置 eureka 注册服务地址
  eureka:
    instance:
      hostname: localhost
    client:
      service-url:
        defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka
      register-with-eureka: false   # 自身不需要注册到 Eureka 中
      fetch-registry: false  # 不需要从 Eureka 中提取数据
  ```

-  访问  Eureka 服务   http:/localhost:8761

<br/>

<br/>

##  Eureka  Client  

>  将一个  微服务 变成  eureka client  ,   目的是  为了 将 微服务 交给  Eureka  进行 统一管理 ，  客户端 将自己 交给  Eureka Server  的过程  被 称为  服务注册。

- 添加 Eureka Client  依赖包
  ```xml
  <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
  </dependency>
  ```

- 在 启动类  /  配置类  添加 @EnableEurekaClient   或者  @EnableDiscoveryClient
  ```java
  @EnableDiscoveryClient
  @SpringBootApplication
  public class OrderApplication {
  
      public static void main(String[] args) {
          SpringApplication.run(OrderApplication.class, args);
      }
  }
  ```

- 注册 微服务 到 Eureka Server  中 
  ```yaml
  spring:
    application:
      name: order   # 微服务的名字
  
  eureka:
    client:
      service-url: 
        defaultZone: http://localhost:8761/eureka    # 注册 EurekServer 的 服务地址
  ```

## Eureka 自我保护机制

> Eureka的自我保护机制是为了防止因为瞬时的网络问题或负载而导致整个服务注册中心崩溃。
> 
> Eureka的自我保护机制的主要特点包括：
> 
> 心跳机制：Eureka客户端会定期发送心跳信号到Eureka服务器，以表明它们仍然活跃。如果Eureka服务器在一段时间内没有收到某个客户端的心跳，它会将该客户端的实例标记为“下线”。
> 
> 自我保护模式：如果Eureka服务器在一段时间内没有收到足够数量的客户端的心跳，它会进入自我保护模式。在自我保护模式下，Eureka服务器会认为所有注册的服务实例都是活跃的，即使它们没有发送心跳。这可以防止因为网络问题或其他问题导致的误报下线。
> 
> 自我保护阈值：Eureka服务器会根据配置的阈值来确定何时进入自我保护模式。阈值通常基于心跳的频率和客户端的数量。如果在指定时间内，收到的心跳数量低于阈值，服务器将进入自我保护模式。
> 
> 恢复：一旦Eureka服务器进入自我保护模式，它会等待一段时间来观察是否恢复正常。如果在此期间有足够数量的心跳，服务器将退出自我保护模式，并将不再认为所有实例都是活跃的。
> 
> 自我保护机制的目标是保持服务注册表的稳定性，即使在临时网络问题或服务实例负载不均的情况下也能够正常工作。但是，长时间处于自我保护模式可能表明存在更严重的问题，因此仍然需要监控和排除潜在的故障。

- eureka.server.enable-self-preservation（默认为true）：用于启用或禁用自我保护模式。如果设置为true，则自我保护模式开启，如果为false，则关闭。通常情况下，应该保持默认值为true，以启用自我保护机制。
- eureka.server.eviction-interval-timer-in-ms（默认为60000毫秒）：这是Eureka服务器用来清理已经下线实例的定期任务的时间间隔。在自我保护模式下，即使实例没有发送心跳，它们仍然会保留在注册表中，直到超过此时间间隔。
- eureka.server.renewal-percent-threshold（默认为0.85）：指定心跳的续约百分比阈值，用于触发自我保护机制。如果在一个时间窗口内的心跳数低于注册实例的总数的百分之85（默认值），则Eureka服务器将进入自我保护模式。
- eureka.server.renewal-threshold-update-interval-ms（默认为30000毫秒）：用于更新续约百分比阈值的时间间隔。这个时间间隔用于重新计算续约阈值，以适应动态变化的实例数量。
- eureka.server.enable-registry-sync（默认为true）：启用或禁用Eureka服务器的注册表同步功能。如果设置为true，Eureka服务器会尝试从其他Eureka服务器同步注册表信息。

##  微服务间 调用

-  EurekaClient  配合 RestTemplate 
- RestTemplate  配合  @LoadBalanced 注解 
  ```java
  @Configuration
  public class RootConfig {
  
      @Bean
      @LoadBalanced
      public RestTemplate restTemplate() {
          return new RestTemplate();
      }
  }
  
  
  
  
  @Resource
  private RestTemplate restTemplate ;
  /**
   * 下单
   * @return
   */
  @GetMapping("/buy/{orderNum}")
  public String genneratorOrder(@PathVariable("orderNum") String orderNum) {
      // 获取支付的 接口地址  put请求 
      MultiValueMap<String, Object>  body = new LinkedMultiValueMap<>();
      body.add("orderNum", orderNum);
  
      MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
      headers.set("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);
  
      RequestEntity<MultiValueMap<String, Object>> request
              = new RequestEntity<>(body, headers, HttpMethod.PUT, URI.create("http://PAY/pay"));
  
      ResponseEntity<String> forObject = restTemplate.exchange(request, String.class);
  
      return "订单已生成、订单编号是:" + orderNum + ", "  + forObject.getBody();
  }
  ```

#  OpenFeign 

>  是一个声明式 的 远程调用 客户端 

-  在 消费端 引入  spring-cloud-starter-openfeign 
  ```java
   <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-openfeign</artifactId>
  </dependency>
  ```

- 在 启动类上 添加 一个 注解 @EnableFeignClients
  ```java
  @EnableDiscoveryClient
  @EnableFeignClients
  @SpringBootApplication
  public class OrderApplication {
  
      public static void main(String[] args) {
          SpringApplication.run(OrderApplication.class, args);
      }
  }
  
  ```

- 添加一个 服务层 接口， 定义要调用的 接口 
  ```java
  @FeignClient("PAY")
  public interface PayService {
  
      /**
       * 调用 支付系统的  支付接口，
       *      接口地址是 /pay,
       *      请求方式是 PUT,
       *      请求参数是 表单参数，键为 orderNum,
       *      返回结果： 返回一个 字符串
       */
      @PutMapping("/pay")
      public String payOrder(@RequestParam("orderNum") String orderNum) ;
  
  }
  
  ```

- 在 控制器中 注入  PayService  , 调用  payOrder 方法，实现 远程接口的调用 

```java
@RestController
@RequestMapping("/order")
public class OrderController {

    @Resource
    private PayService payService ;
    /**
     * 下单
     * @return
     */
    @GetMapping("/buy/{orderNum}")
    public String genneratorOrder(@PathVariable("orderNum") String orderNum) {
        // 获取支付的 接口地址
        String payResult = payService.payOrder(orderNum);


        return "订单已生成、订单编号是:" + orderNum + ", "  + payResult;
    }
}
```

<br/>

#  Hystrix 断路器 （豪猪）

学习文档：  https://github.com/Netflix/Hystrix

> 微服务间调用 设置断路器， 起到一个保护作用。 保护整个微服务链路正常工作，较少 雪崩 。

- 服务降级
  - 程序产生异常
  - 服务超时
- 服务限流
- 服务熔断

##  Hystrix 在 生产者中使用 

- 引用依赖包 
  ```xml
  <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
      <version>2.2.10.RELEASE</version>
  </dependency>
  ```

-  在 启动类 上添加 注解  @EnableHystrix 

- 在 需要 保护的  接口上， 添加 对应的 注解  @HystrixCommand、实现 服务降级 、限流、 熔断等技术 
  - 全局方法  进行服务降级  (defaultFallback 指定的方法 ， 返回值  必须 和  目标方法 完全相同，  但不需要提供任何的参数)
  ```java
  @DefaultProperties(defaultFallback = "payFallback", commandProperties = {
          @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000"),
          @HystrixProperty(name = "execution.timeout.enabled", value = "true"),
          @HystrixProperty(name="execution.isolation.thread.interruptOnTimeout", value="true")
  })
  ```
  - 局部方法 进行服务器降级  （fallbackMethod 指定的方法 ， 返回值 、 参数  必须 和  目标方法 保持 完全相同）
  ```java
  @HystrixCommand(fallbackMethod = "payFallback",
        commandProperties = {
                @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000"),
                @HystrixProperty(name = "execution.timeout.enabled", value = "true"),
                @HystrixProperty(name="execution.isolation.thread.interruptOnTimeout", value="true")
        })
  ```

## Hystrix 在 消费者中使用  (在 openfeign 中 使用 hystrix 进行服务降级)

- 引用依赖包 

  ```xml
  <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
      <version>2.2.10.RELEASE</version>
  </dependency>
  ```

- 在 启动类 上添加 注解  @EnableHystrix
- 在  application.yml 中，  启用  openfeign  对 hystrix 的支持 
  ```yaml
  feign:
    client:
      config:
        PAY:   #针对调用的哪一个微服务进行的配置
          connectTimeout: 1000
          readTimeout: 2000
    circuitbreaker:
      enabled: true
  ```

<br/>

# Hystrix Dashboard (仪表盘)

-  引入依赖包 
  ```xml
  <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-netflix-hystrix-dashboard</artifactId>
      <version>2.2.10.RELEASE</version>
  </dependency>
  ```

- 编写 启动类 、并添加对应的注解 
  ```java
  @EnableHystrixDashboard
  @SpringBootApplication
  public class HystrixDashboardApplication {
  
      public static void main(String[] args) {
          SpringApplication.run(HystrixDashboardApplication.class, args);
      }
  }
  
  ```
