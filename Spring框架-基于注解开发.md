# 控制反转创建注解

- @Controller   :  应用于 控制层
- @Service   :  应用于 业务路基层
- @Repository  :  应用于 持久层 （如果使用的是 mybatis框架， 则该注解用不上）
- @Component ：  通用注解，可以用在 任意层， 但推荐用在 除 控制层、业务层、持久层 之外的 其他层 
- @Named :   JSR-330 标准 注解 

**上述的四个注解作用是等价的、都是注解到类上的。 只是 开发人员为了区分 不同的层、提供了不同的注解而已。** 

- @Scope  :   设置 bean 的作用范围 ， 默认是 单例 模式，   如果要使用多例模式，需要设置 为  @Scope("prototype") 
- @Primary :  是否是 主要的 bean 
- DependsOn :  设置 该 对象 依赖与 某一个 对象 

-  InitializingBean 接口 ，  该接口 中提供的方法 afterPropertiesSet  会在 所有的属性 被依赖注入完成后 执行的代码，  可以做一些 属性校验 等工作 ， 类似于 XML 中 init-method 配置 
- DisposableBean 接口 ， 该接口 中提供的方法 destroy  会在 对象 销毁前 执行 一段 逻辑 、 类似于 XML 中 的 destroy-method 配置 
- @PostConstruct ：  基于 JSR-250 规范 、等价于 init-method  ,  需要添加  javax.annotation-api  依赖库
- @PreDestroy :   基于 JSR-250 规范， 等价于 destroy-method

## 依赖注入 常用的注解 

- @Value :    给 字面量 对应的 属性 注入 值  ，  可以 应用在 Field (属性上）  、 setter 方法 （Property属性上） 、 构造方法的 参数 上 ， 还可以使用 ${key} 的语法 从 配置文件中 获取值 并注入到对应的 属性中 。
- @Autowired ：  
  - 如果写在 构造方法上， 用来标记 该构造方法 作为 Spring 管理对象 调用的 构造方法 。 如果 一个类中 没有提供任何构造方法，那么 会默认使用 无参构造方法创建对象，  如果一个类中 提供了 无参构造 和 有参构造 ， 但没有提供 @Autowired，
    
    那么 会使用 无参构造创建对象， 如果 类中 有且只有 一个 有参构造， 那么 会使用 该 构造方法 尝试 对象。 如果有多个有参，建议 使用 @Autowired 进行标记
  - 可以 用来 注入 引用 对象 ，  可以写在 属性上 、setter 方法上  。  默认 是 按照 注入属性的 类型 从 Spring容器中查找的， 如果 Spring容器中 没有该对象， 会直接抛出异常，可以通过  required 属性设置为 false 解决该错误 。 如果 容器中 有多个该类型的对象， 且没有使用 primary 进行 标记， 也会 报错。  可以 通过 @Qualifier 注解 通过 指定的 名字 注入 对象 。

- @Resource :   该注解不是 Spring 提供的注解、 该注解是 JSR-250 规范中提供的注解
  - 该注解 不能写在构造方法 和 方法的参数上， 所以 不能实现 构造注入 
  - @Resource 注解 默认 按照 名字 注入 对象，  可以 通过 name 设置要注入的 bean的 ID,    如果 设置的 name , 则只能按照 name 进行注入， 如果容器中找不到，则会抛出异常。 如果 没有设置 name ,   那么 首先 会 根据 注释的 属性名 从 容器中 查找对象， 如果找到 ，则 直接注入 ，  如果找不到 ，  此时 会根据 类型 查找 ， 如果 该类型 在容器中有多个， 则 抛出异常， 如果找不到，也会抛出异常 。

###  @Resource  VS  @Autowired  VS  @Inject

||@Resource|@Autowired|@Inject|
|--|--|--|--|
|使用范围|属性、 Setter 方法上|构造方法上、属性上、 Setter方法上、 构造方法的参数上|构造方法、属性、 Setter方法上|
|构造注入|不能实现构造注入|可以使用 构造注入|可以使用 构造注入|
|注入方式|默认按照名字注入，当名字找不到，按照类型注入。|默认按照 类型查找 、可以配置 @Qualifier 实现按照名字查询|默认按照名字查找，但却不能指定名字，默认名为注释的属性名，找不到，按类型查找<br/>可以 配合 @Named  按照 指定名字查找|
|必须注入|找不到，或者找到多个，均会抛出异常。|默认必须注入 ， 但可以通过 @Autowired(required=false)  设置，找不到也不会报错。|找不到，或者找到多个，均会抛出异常。|
|提供方|JSR-250|Spring官方提供的|JSR-330|

<br/>

## Aspectj  切面编程

- 添加  aspectj 相关依赖 
  ```xml
  <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-aop</artifactId>
      <version>5.3.27</version>
  </dependency>
  
  <!--  添加 aop aspectj 切面的支持 -->
  <dependency>
      <groupId>org.aspectj</groupId>
      <artifactId>aspectjweaver</artifactId>
      <version>1.9.19</version>
  </dependency>
  ```

- 启用 Aspectj 注解的支持 
  ```xml
  <aop:aspectj-autoproxy proxy-target-class="false" />
  ```

		  proxyTargetClass 默认值为 false,  代表 采用 JDK 动态代理 ，  如果设置为  true , 代表 采用 CGLIB 动态代理 

 		 jdk 动态代理：  要代理的目标对象必须实现接口， 且 返回的 代理 对象 和 目标对象是 兄弟关系 

  		cglib 动态代理 ： 要代理的目标对象可以不用实现任何借口 ， 且 返回的 代理对象  是 目标对象 的 子类对象。

- 编写 增强类 
  ```java
  @Aspect
  @Component
  public class TimerAspectj {
  
  
      @Pointcut("execution(* com.haredot.service..*.*(..))")
      public void before() {}
  
      @Before("before() && this(service)")
      public void before(JoinPoint joinPoint, Object service) {
          System.out.println("目标对象是:" + service);
          String sign = joinPoint.getSignature().toString();
          System.out.println("准备执行" + sign + "-----------");
      }
      @AfterReturning(value = "before()", returning = "ret")
      public void after(JoinPoint joinPoint, Object ret) {
          System.out.println("方法执行完成，返回结果是:" + ret);
      }
  
      @AfterThrowing(value = "before()", throwing = "e")
      public void exception(JoinPoint joinPoint, Throwable e) {
          System.out.println("产生了异常，异常信息是：" + e.getMessage());
      }
  
      @After(value = "before()")
      public void after(JoinPoint joinPoint) {
          System.out.println("无论成功还是失败、都会执行的代码...............");
      }
  
      @Around(value = "before()")
      public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
          // 前置
          System.out.println("around before");
          Object ret = joinPoint.proceed();
          // 后置
          System.out.println("around after");
          return ret ;
      }
  }
  ```

#  Spring 声明式事务管理

- 引入 依赖包  ` spring-tx,  spring-jdbc`  依赖包  或者 引入 spring-orm 替代 
  ```xml
  <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-tx</artifactId>
      <version>5.3.27</version>
  </dependency>
  
  <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-jdbc</artifactId>
      <version>5.3.27</version>
  </dependency>
  ```
- 配置 事务管理器 
  ```xml
  <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
      <property name="dataSource" ref="dataSource" />
  </bean>
  ```

- 启用 事务注解的支持 
  ```xml
  <tx:annotation-driven transaction-manager="transactionManager"/>
  ```

- 在 业务层 使用 @Transactional 注解 
  ```java
  @Service
  @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class, isolation = Isolation.READ_COMMITTED)
  public class UserServiceImpl implements UserService {
  
      @Override
      @Transactional
      public int save() {
          System.out.println("正在插入用户....");
          return 1 ;
      }
  }
  ```

	**当类上i提供了@Transactional， 那么所有 公开的方法上都会自动添加注解支持， 方法上也可以使用该注解， 如果类和方法上都有，则方法会覆盖类上的注解特点**
