#  JavaConfig 配置类 

>  Java Config  工作模式 就是 将所有 需要管理的 对象 在 配置类中 进行管理 。这个配置类的作用 类似于 Spring 容器。

-  @Configuration  ：  该注解 标记一个类 是 配置 类 
- @Bean :  该注解 应用在 配置类中对应的方法上， 用来标记 该方法 返回的结果 作为 Spring 管理的对象 ,  能够 实现 控制反转。
- @Import :  可以导入 其他 配置类 ， 被导入的配置类 上 可以 不添加 @Configuration 注解 
- @ImportResource("classpath:spring-context.xml")  ： 读取 指定的 Spring 容器 
- @ComponentScan("com.haredot")  ：  替代 <context:component-scan /> 标签 ，负责扫描包
- @PropertySource(value = "classpath:jdbc.properties",  encoding = "UTF-8")  :  读取 properties  配置文件 

## Aspectj  切面编程

- 添加  aspectj 相关依赖 
  ```xml
  <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-aop</artifactId>
      <version>5.3.27</version>
  </dependency>
  
  <!--  添加 aop aspectj 切面的支持 -->
  <dependency>
      <groupId>org.aspectj</groupId>
      <artifactId>aspectjweaver</artifactId>
      <version>1.9.19</version>
  </dependency>
  ```

- 启用 Aspectj 注解的支持 
  ```xml
  @EnableAspectJAutoProxy(proxyTargetClass = false)
  ```
  
  proxyTargetClass 默认值为 false,  代表 采用 JDK 动态代理 ，  如果设置为  true , 代表 采用 CGLIB 动态代理 
  
  jdk 动态代理：  要代理的目标对象必须实现接口， 且 返回的 代理 对象 和 目标对象是 兄弟关系 
  
  cglib 动态代理 ： 要代理的目标对象可以不用实现任何借口 ， 且 返回的 代理对象  是 目标对象 的 子类对象。
- 编写 增强类 
  ```java
  @Aspect
  @Component
  public class TimerAspectj {
  
  
      @Pointcut("execution(* com.haredot.service..*.*(..))")
      public void before() {}
  
      @Before("before() && this(service)")
      public void before(JoinPoint joinPoint, Object service) {
          System.out.println("目标对象是:" + service);
          String sign = joinPoint.getSignature().toString();
          System.out.println("准备执行" + sign + "-----------");
      }
      @AfterReturning(value = "before()", returning = "ret")
      public void after(JoinPoint joinPoint, Object ret) {
          System.out.println("方法执行完成，返回结果是:" + ret);
      }
  
      @AfterThrowing(value = "before()", throwing = "e")
      public void exception(JoinPoint joinPoint, Throwable e) {
          System.out.println("产生了异常，异常信息是：" + e.getMessage());
      }
  
      @After(value = "before()")
      public void after(JoinPoint joinPoint) {
          System.out.println("无论成功还是失败、都会执行的代码...............");
      }
  
      @Around(value = "before()")
      public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
          // 前置
          System.out.println("around before");
          Object ret = joinPoint.proceed();
          // 后置
          System.out.println("around after");
          return ret ;
      }
  }
  ```

# Spring 声明式事务管理

- 引入 相关依赖包 
- 配置 事务管理器 
  ```java
  @Configuration
  public class TransactionalAutoConfig {
      @Bean
      public TransactionManager transactionManager(@Qualifier("dataSource") DataSource dataSource) {
          return new DataSourceTransactionManager(dataSource);
      }
  }
  
  ```

- 启用 注解支持 
  ```java
  @EnableTransactionManagement
  public class TransactionalAutoConfig {
  
      @Bean
      public TransactionManager transactionManager(@Qualifier("dataSource") DataSource dataSource) {
          return new DataSourceTransactionManager(dataSource);
      }
  }
  ```

- 在业务层使用@Transactional注解
  ```java
  @Service
  @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class, isolation = Isolation.READ_COMMITTED)
  public class UserServiceImpl implements UserService {
  
      @Override
      @Transactional
      public int save() {
          System.out.println("正在插入用户....");
          return 1 ;
      }
  }
  ```
