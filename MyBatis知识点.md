# MyBatis 介绍

MyBatis（前身为iBATIS）是一种Java持久化框架，用于简化数据库访问的开发。它提供了一种将SQL语句与Java代码解耦的方式，通过XML文件或注解来配置和映射数据库操作。

以下是MyBatis框架的一些重要概念和特点：

数据库映射：MyBatis允许你将Java对象与数据库表之间进行映射。通过定义映射规则，你可以将查询结果映射到Java对象中，或将Java对象的属性值插入到数据库表中。

SQL语句控制：使用MyBatis，你可以在XML文件或注解中编写SQL语句，并将其与Java方法进行绑定。这使得你可以更好地控制SQL语句的编写和组织。

参数绑定：MyBatis允许你在SQL语句中使用参数，并将其与Java方法的参数进行绑定。这样，你可以轻松地向SQL语句中传递参数。

缓存支持：MyBatis提供了一级缓存和二级缓存的支持。一级缓存是会话级别的缓存，它可以减少对数据库的重复查询。二级缓存是全局级别的缓存，可以跨会话共享数据，提高性能。

插件扩展：MyBatis提供了插件机制，允许你在执行SQL语句的过程中添加自定义的功能。你可以编写自己的插件来扩展MyBatis的功能，例如日志记录、性能监控等。

MyBatis 是一个 基于 ORM 设计思想 的 半自动化 框架  

## ORM 对象关系映射

解决问题：   实体类 和 表 的映射关系 ，  实体对象 和 表 记录 的映射关系 ，  实体类中的属性 和 表 中的 字段的映射关系

<br/>

## MyBatis 框架搭建 

- 引入 依赖包 
  ```xml
  <dependency>
      <groupId>org.mybatis</groupId>
      <artifactId>mybatis</artifactId>
      <version>3.5.13</version>
  </dependency>
  
  <dependency>
      <groupId>com.mysql</groupId>
      <artifactId>mysql-connector-j</artifactId>
      <version>8.0.33</version>
  </dependency>
  ```

- 在 资源目录 CLASSPATH 下 新建一个  mybatis-config.xml (名字可随意) 配置文件 
  - DTD 头信息
    ```xml
    <?xml version="1.0" encoding="UTF-8" ?>
    <!DOCTYPE configuration
            PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
            "http://mybatis.org/dtd/mybatis-3-config.dtd">
    
    <configuration>
       ...
    
    </configuration>
    ```
  - 读取 properties 配置文件
    ```xml
     <properties resource="jdbc.properties">
        <!--  启用 OGNL 表达式 支持 设置 默认值, 默认分隔符是 :      -->
        <property name="org.apache.ibatis.parsing.PropertyParser.enable-default-value" value="true"/>
        <property name="org.apache.ibatis.parsing.PropertyParser.default-value-separator" value="??"/>
    </properties>
    ```
  -  管理映射文件
    ```xml
    <!-- 方式一 -->
    <mappers>
        <!--  从 classpath 下 加载 对应的 映射文件， 每一个映射文件都要进行配置   -->
        <mapper resource="mapper/GoodsMapper.xml"></mapper> 
    </mappers>
    
    
    <!-- 方式二 -->
    <mappers>
        <!--  从指定的包下读取映射文件， 注意需要满足以下要求： a) 映射文件和持久层接口需要在同一个包下， b) 持久层接口 类名 和 映射文件 文件名需要保持一致   -->
        <package name="com.qikux.mapper"> 
    </mappers>
    ```
  - 全局设置 
    ```xml
    <settings>
        <!--   将蛇形命名 自动映射为 驼峰命名   -->
        <setting name="mapUnderscoreToCamelCase" value="true"/>
        <!--  解决查询的字段值为null, 返回 Map 字段不显示的问题   -->
        <setting name="callSettersOnNulls" value="true"/>
        
        <!--    配置日志类型    -->
        <setting name="logImpl" value="SLF4J"/>
        <!-- 启用 二级缓存 -->
        <setting name="cacheEnabled" value="true"/>
    </settings>
    ```
  - 别名配置
    ```xml
    <!-- 方式一 -->
    <typeAliases>
        <!--   定义 Goods 实体类的别名 ， 多个类 需要 每个都要通过 typeAlias 进行定义    -->
       <typeAlias type="org.haredot.entity.Goods" alias="goods" />
    </typeAliases>
    
    <!-- 方式二 -->
    <typeAliases>
        <!-- 如果使用 package 来定义别名，那么 框架提供的别名是对应的类名(不区分大小写) -->
        <package name="org.haredot.entity"/>
        <package name="org.haredot.dto"/>
    </typeAliases>
    ```
  - 数据源配置 
    ```xml
    <environments default="dev">
        <!--  一个 environment 就是一个数据源， 通过 id 给数据源设置一个唯一标识   -->
        <environment id="dev">
            <!--    配置事务管理的类型 , 值为 JDBC    -->
            <transactionManager type="JDBC" />
            <!--   type 用来定义 数据源的类型， 通常会使用连接池 POOLED   -->
            <dataSource type="POOLED">
                <!--    一个 property 代表数据源的一个属性 , value 支持 OGNL表达式，${key}     -->
                <property name="driver" value="${db.driver}" />
                <property name="url" value="${db.url}"/>
                <property name="username" value="${db.username??root}"/>
                <property name="password" value="${db.password}"/>
                <!--       池相关的属性，可配 可不配         -->
                <property name="poolMaximumActiveConnections" value="20"/>
                <property name="poolPingEnabled" value="true"/>
                <property name="poolPingQuery" value="select 1"/>
            </dataSource>
        </environment>
    </environments>
    
    ```

- 映射文件
  - DTD 头信息
    ```xml
    <?xml version="1.0" encoding="UTF-8" ?>
    <!DOCTYPE mapper
            PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
            "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
            
    <mapper namespace="">
    
    
    </mapper>
    ```
  - 命名空间 
    > namespace 是为了区分 映射文件中定义的 ID的唯一性的， 不同的映射文件，命名空间不一样， 如果采用 持久层接口编程，那么 命名空间名字 必须是 持久层接口的 全名(包名 + 类名
    ```xml
    <mapper namespace="com.qikux.mapper.GoodsMapper">
    
    </mapper>
    ```
  - update 标签
    ```xml
    <update id="update">
       update tb_goods set name = #{name}, price = #{price} where id = #{id}
    </update>
    ```
  - delete 标签
    ```xml
    <delete id="removeById">
        delete from tb_goods where id = #{id}
    </delete>
    ```
    >  如果参数的类型是 字面量(基本数据类型、包装类、字符串)类型， 那么 占位符#{key} 中的 key 可以任意。
  - select 标签 
    ```xml
    <select id="findGoodsById" resultType="goods">
        select * from tb_goods where id = #{id}
    </select>
    ```
    >  必须 提供 resultType  或者  resultMap  用来 处理 返回的 类型
  - insert  标签
  ```xml
  <!-- parameterType 用来设置 参数的类型，可以省略不写   -->
  <insert id="saveGoods"  parameterType="goods">
      insert into tb_goods(name, price, create_time) values
      (#{name}, #{price}, now())
  </insert>
  
  
  
  <!-- 插入并获取主键 方式一： -->
  <insert id="saveGoods">
      insert into tb_goods(name, price, create_time) values
      (#{name}, #{price}, now())
  
      <!--  获取插入的主键
              keyProperty : 属性名： 查询到的主键 要存储到 方法参数 对应的 属性中
              resultType :  用来设置查询的结果对应的类型
                  double , float, long , int , short , byte , boolean, char , string,  list , map, array (包装类对应的类型)
                  _double, _float, _long , _int, _short, _byte ...  (基本数据类型)
      -->
      <selectKey keyProperty="id" resultType="long" order="AFTER" keyColumn="id">
          select last_insert_id()
      </selectKey>
  </insert>
  
  
  
  <!-- 插入并获取主键 方式二： -->
  <insert id="save"  useGeneratedKeys="true" keyProperty="id" keyColumn="id">
       insert into tb_goods(name, price, create_time) values
      (#{name}, #{price}, now())
  </insert>
  ```

## @Param 注解 

>  在 持久层接口中，可以对 传入的参数 使用 @Param 注解进行 注释 ， 该注解 配合 映射文件中的 #{key}  使用
> 
> 如果持久层 接口 方法的参数 只有一个， 可以 不提供 @Param注解，  但如果有多个参数，建议 添加 @Param 注解， 如果此时没有改注解，那么占位符 取值 需要通过 param1 , param2 , ... 来获取对应的参数 

##  SQL片段

-  定义 SQL 片段 

```xml
<sql id="columns">id , username,  password </sql>
```

- 使用 SQL 片段 

```xml
<include  refid="columns" />
```

- SQL 片段 绑定参数 

```xml
<sql id="columns">${p}.id , ${p}.username,  ${p}.password </sql>

<include  refid="columns">
   <property name="p" value="u" />  
</include>
```

## 动态SQL

-  if  
  ```xml
  <update id="patch">
      update tb_goods
      <trim prefix="set" suffixOverrides=",">
          <if test="name != null and name != ''" >
              name = #{name} ,
          </if>
    
          <if test="price != null and price != ''">
              price = #{price} ,
          </if>
      </trim>
      where id = #{id}
  </update>
  ```
- choose  ... when ... otherwise
  ```xml
  <select id="findGoodsByCondition" resultType="goods">
      select <include refid="columns" /> from tb_goods
  
      <!--     可以代替 where , set 标签，能够实现更加强大的功能       -->
      <!--  如果 传入了 商品名， 则按照商品名查找 ， 如果传入了 价格， 则按照价格查找，否则 按照 ID 降序排列      -->
      <choose>
          <when test="goods.name != null and goods.name != ''">
              <bind name="lname" value="'%' + goods.name + '%'"></bind>
              where name like #{lname}
          </when>
  
          <when test="goods.price != null and goods.price != ''">
              where price = #{goods.price}
          </when>
  
          <otherwise>
               order by id desc
          </otherwise>
      </choose>
  </select>
  ```
- foreach
  ```xml
  <delete id="removeBatch">
       delete from tb_goods where id in
      <!--
          collection : 设置要遍历的 集合对象
             a) List ： 如果参数是 List, 且没有使用 @Param 注解， 那么 在 collection 中添加 list
             b) Set  ：  如果参数是 Set, 且没有使用 @Param 注解， 那么 在 collection 中添加 set
             c) 数组  :  如果参数是 数组, 且没有使用 @Param 注解， 那么 在 collection 中添加 array
  
          item :  用来表示 集合中 每一项 存储的 变量
          open :  整个循环 以 ... 开始 (如果没有需要，可不填)
          close : 整个循环 以 ... 结尾 (如果没有需要，可不填)
          separator ： 遍历中 多个值 以 ... 分割
          index : 遍历集合中对应元素的 索引值 (不常用)
       -->
       <foreach collection="array" item="id" open="(" close=")" separator=",">
          #{id}
      </foreach>
  </delete>
  ```
- set 
- where
- trim

## 高级映射 ResultMap

- 基础用法 ： 解决 查询字段 和属性 不一致的问题
- 映射 集合属性 
  - 方式一 
    ```xml
    
    <resultMap id="userResourceMap" type="userResourcesDTO" autoMapping="true">
        <id property="id" column="id" />
        <!--
            collection 标签将多条记录注入到一个 List , Set, Collection 类型的 属性中
            ofType ： 用来设置 单值集合 中 元素的类型
         -->
        <collection property="resources" ofType="resource" autoMapping="true">
            <!--     解决查询的SQL字段和 属性不一致的问题       -->
            <id property="id" column="rid" />
            <result property="createTime" column="rcreate_time"/>
            <result property="updateTime" column="rupdate_time"/>
            <result property="deleteTime" column="rdelete_time"/>
            <result property="del" column="rdel"/>
        </collection>
    
    </resultMap>
    
    
    <select id="findUserByUID" resultMap="userResourceMap">
        select u.* , r.id rid, r.resource_name, r.resource_url, r.keywords, r.type_id, r.score, r.description,
               r.user_id , r.size, r.ext, r.sift,  r.reason , r.create_time as rcreate_time ,
               r.update_time as rupdate_time , r.delete_time as rdelete_time , r.del as rdel
    
        from tb_user u left join tb_resource r on u.id = r.user_id
        where u.id = #{id}
    
    </select>
    ```
  - 方式二
    ```javascript
    <resultMap id="dtoMap" type="userResourcesDTO" autoMapping="true">
        <id property="id" column="id" />
    
        <!--
            使用 select 属性  调用 查询 资源的 SQL
            使用 column 属性设置 通过 那个字段 去 查询 对应的 SQL
        -->
        <collection property="resources" ofType="resource"
                select="findResourceByUserId" column="id" />
    </resultMap>
    
    
    <select id="findUserByUserID" resultMap="dtoMap">
        select u.* from tb_user u where id = #{id}
    </select>
    
    <select id="findResourceByUserId" resultType="resource">
          select * from tb_resource where user_id = #{userId}
    </select>
    
    ```
- 映射 实体对象
  - 方式一
    ```xml
    <resultMap id="resourceMap" type="resourceDTO" autoMapping="true">
        <id column="rid" property="id" />
        <result property="createTime" column="rcreate_time"/>
        <result property="updateTime" column="rupdate_time"/>
        <result property="deleteTime" column="rdelete_time"/>
        <result property="del" column="rdel"/>
        <!--    处理 一 的关系属性    -->
        <association property="user" autoMapping="true">
            <id column="id" property="id" />
        </association>
    </resultMap>
    
    
    <select id="findResourceById" resultMap="resourceMap">
        select r.id rid, r.resource_name, r.resource_url, r.keywords, r.type_id, r.score, r.description,
        r.user_id , r.size, r.ext, r.sift,  r.reason , r.create_time as rcreate_time ,
        r.update_time as rupdate_time , r.delete_time as rdelete_time , r.del as rdel  , u.*
        from tb_resource r left join tb_user u on r.user_id = u.id
        where r.id = #{id}
    </select>
    ```
  - 方式二
    ```xml
    <resultMap id="resourceMap2" type="resourceDTO" autoMapping="true">
        <id column="id" property="id" />
        <association property="user" select="findUserById"
                column="user_id" fetchType="lazy"/>
    </resultMap>
    
    
    <select id="findResources" resultMap="resourceMap2">
        select * from tb_resource
    </select>
    
    
    <select id="findUserById" resultType="user">
            select u.*  from tb_user u where u.id = #{id}
    </select>
    ```

##  PageHelper 分页

- 引入依赖 
  ```xml
  <dependency>
      <groupId>com.github.pagehelper</groupId>
      <artifactId>pagehelper</artifactId>
      <version>5.3.3</version>
  </dependency>
  ```

- 在 mybatis 配置 文件中，添加 分页插件 
  ```xml
  <plugins>
      <plugin interceptor="com.github.pagehelper.PageInterceptor">
          <!-- 使用下面的方式配置参数，后面会有所有的参数介绍 -->
          <property name="helperDialect" value="mysql"/>
          <property name="defaultCount" value="true"/>
          <!--  如果设置为true, 且 pageSize(每页显示的条数) 如果为 0 ，则 不进行分页    -->
          <property name="pageSizeZero" value="false"/>
          <!--
              reasonable 如果设置 为 true, 则代表  合理进行分页显示
  
              例如 当前是 第一页， 点击上一页后 ， 仍旧显示 第一页的数据
  
              当前页 是 最后一夜， 点击 下一页后， 仍旧显示 最后一页的数据
           -->
          <property name="reasonable" value="false"/>
      </plugin>
  </plugins>
  ```

- 代码中实现 分页查询 
  ```java
  // 1. 开启 分页查询
  PageHelper.startPage(1, 5) ;
  
  // 2. 调用 SQL 查询数据
  List<User> users = mapper.findUsers();
  
  // 3.  构建一个 PageInfo 对象， 将查询到的数据 放到 PageInfo 对象中， 该对象包含 了分页需要的所有数据。
  PageInfo<User> info = new PageInfo<>(users);
  
  // 所有和分页相关的信息 全部都在 pageInfo 中可以找到
  // 获取 当前页
  System.out.println("info.getPageNum() = " + info.getPageNum());
  // 获取每页显示条数
  System.out.println("info.getPageSize() = " + info.getPageSize());
  // 获取总页数
  System.out.println("info.getPages() = " + info.getPages());
  // 获取 总条数
  System.out.println("info.getTotal() = " + info.getTotal());
  
  // 是否有上一页
  System.out.println("info.isHasPreviousPage() = " + info.isHasPreviousPage());
  // 是否有下一页
  System.out.println("info.isHasNextPage() = " + info.isHasNextPage());
  // 获取下一页页码
  System.out.println("info.getNextPage() = " + info.getNextPage());
  // 获取上一页页码
  System.out.println("info.getPrePage() = " + info.getPrePage());
  ```

- 继承 IPage 接口， 实现分页
  ```java
  import com.github.pagehelper.IPage;
  
  public class PageVo implements IPage {
      /**
       * 当前页码
       */
      private Integer page ;
      /**
       * 每页显示条数
       */
      private Integer rows ;
      /**
       * 排序的字段名
       */
      private String column ;
      /**
       * 排序方式
       */
      private Order order ;
  
      public PageVo() {}
  
      public PageVo(Integer page, Integer rows) {
          this.page = page;
          this.rows = rows;
      }
  
      public PageVo(Integer page, Integer rows, String column, String order) {
          this.page = page;
          this.rows = rows;
          this.column = column;
          this.setOrder(order);
      }
  
      public void setPage(Integer page) {
          this.page = page;
      }
  
      public void setRows(Integer rows) {
          this.rows = rows;
      }
  
      public void setColumn(String column) {
          if (!column.matches("\\w+")) {
              throw new RuntimeException("参数column对应的值格式不正确") ;
          }
          this.column = column;
      }
  
      public void setOrder(String order) {
          order = order.toUpperCase() ;
          if (order.equals("ASC") || order.equals("DESC")) {
              this.order = Order.valueOf(order);
          }
      }
  
      @Override
      public Integer getPageNum() {
          return page;
      }
  
      @Override
      public Integer getPageSize() {
          return rows;
      }
  
      @Override
      public String getOrderBy() {
          if (this.column !=null) {
              return this.column + " " + (order == null ? Order.ASC.name() : order.name());
          }
          return null ;
      }
  
      /**
       * 枚举类
       */
      public enum Order {
          ASC , DESC
      }
  }
  
  
  // 方式一
  PageInfo<User> objectPageInfo = PageHelper.startPage(1,  5)
          .doSelectPageInfo(() -> mapper.findUsers());
  
  
  // 方式二
  PageInfo<User> objectPageInfo = PageHelper.startPage(new PageVo(1, 5))
          .doSelectPageInfo(() -> mapper.findUsers());
  
  
   // 方式三
  PageInfo<User> objectPageInfo = PageHelper.startPage(new PageVo(1, 5, "id" , "desc"))
          .doSelectPageInfo(() -> mapper.findUsers());
  
  ```

##  缓存

###  SqlSession 一级缓存

### SqlSessionFactory 二级缓存

## MyBatis 基于JDBC 实现 批量操作

```java
SqlSessionFactory sf = new SqlSessionFactoryBuilder().build(in);

//3. 获取 支持 批量处理的 SqlSession 对象
SqlSession sqlSession = sf.openSession(ExecutorType.BATCH);

List<Goods> list = List.of(Goods.builder().name("AAAA").price("220").build(),
        Goods.builder().name("BBBB").price("4440").build(),
        Goods.builder().name("CCCC").price("3440").build());

GoodsMapper mapper = sqlSession.getMapper(GoodsMapper.class);

for (Goods goods : list) {
    mapper.save(goods);
}

// 进行批量插入
sqlSession.flushStatements();

sqlSession.commit();

sqlSession.close();
```
