#  SpringBoot 的特点

-  内置 tomcat,   不需要进行 额外部署，可直接运行 
- 使用 @EnableAutoConfiguration 注解 ,  实现自动配置 
- 提供 健康指标、性能监控 等手段 
- 采用 约定大于配置 手段、减少 配置

# SpringBoot 环境搭建

-  新建一个 maven 工程， 并添加 相关依赖

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.7.14</version>
    </parent>

    <groupId>com.haredot</groupId>
    <artifactId>springboot</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <maven.compiler.source>17</maven.compiler.source>
        <maven.compiler.target>17</maven.compiler.target>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>

</project>
```

- 编写 控制器 

```java
@RestController
public class IndexController {

    @GetMapping("/test")
    public Map<String, Object> test() {
        return Map.of("id" ,  1,  "username",  "张三"); 
    }
}
```

- 编写 SpringBoot 启动类 

```java
@Configuration
@EnableAutoConfiguration
@ComponentScan
public class Application {
    
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
```

> 可以使用 @SpringBootApplication 注解 替代  @Configuration,  @EnableAutoConfiguration,  @ComponentScan 注解 ,  SpringBootApplication 注解是一个 复合注解。

- 访问项目    http://localhost:8080/test

<br/>

#  SpringBoot 自定义属性配置

- 添加 依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-configuration-processor</artifactId>
</dependency>
```

- 编写 配置类 

```java
@ConfigurationProperties(prefix = "security.jwt.config")
public class JwtConfig {
    /**
     * 令牌的过期时间
     */
    private long exp ;

    /**
     * 刷新令牌过期时间
     */
    private long refreshExp ;

    /**
     * 令牌密钥
     */
    private String key ;

    /**
     * 令牌签发者
     */
    private String iss ;
    /**
     * 令牌过期时间的单位
     */
    private TimeUnit unit ;
    
    // 省略了 get /  set 方法    
}
```

>  在 配置 类上， 使用 @ConfigurationProperties  设置 自定义配置的 前缀 

- 在  application.yml  中 ，进行 自定义属性的 配置 

```yaml
security:
  jwt:
    config:
      exp: 5
      unit: minutes
      refresh-exp: 1440
      key: "&0*085234%&"
```

- 使用 JwtConfig  配置类 

```java
@RestController
@EnableConfigurationProperties(JwtConfig.class)
public class IndexController {

    @Resource
    private JwtConfig jwtConfig ;

    @GetMapping("/config")
    public JwtConfig user() {
        return jwtConfig ;
    }
}

```

>  @EnableConfigurationProperties  用来启用 自定义配置属性的支持 ， 如果 不传入  JwtConfig.class 类 ， 那么  JwtConfig 这个 类 必须使用  @Component 注解 进行控制反转
> 
> @EnableConfigurationProperties  还可以设置在 启动类Application上 ,  如果采用该模式，不推荐传入对应的类， 此时 要求所有的配置类 都需要进行 控制反转。

<br/>

# SpringBoot 条件Bean 

>  Contional  是一种 机制， 可以根据 不同的条件， 注入 不同的 对象， 只有当对象 在 条件成立的时候， 该 Bean 才会 生效。  该技术 可以 实现 不同环境 的不同配置 ，可以实现一些 默认配置 

-  ConditionalOnBean  ： 如果指定的 bean 存在 ，则 生效 
- ConditionalOnMissingBean ：  如果指定的 bean 不存在 ，则 生效
- ConditionalOnClass  ：  如果指定的 类 存在， 则生效 
- ConditionalOnMissingClass ：  如果指定的 类 不存在， 则生效
- ConditionalOnProperty ：  如果 指定的 配置 属性 存在，且满足条件， 则生效。
- ConditionalOnJava  ：  根据 JDK 的版本 ，匹配 则 生效 

<br/>

# SpringBoot 整合 mybatis 

- 添加 依赖包  （SpringBoot 默认使用 了 HikariCP 数据库连接池、如果需要使用 druid 可以自行添加依赖包）

```xml
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>2.3.1</version>
</dependency>

<dependency>
    <groupId>com.github.pagehelper</groupId>
    <artifactId>pagehelper-spring-boot-starter</artifactId>
    <version>1.4.7</version>
</dependency>

<dependency>
    <groupId>com.mysql</groupId>
    <artifactId>mysql-connector-j</artifactId>
</dependency>
```

- 配置 数据库 链接 信息 

```yaml
spring:
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/rbac
    username: root
    password: 123456
    type: com.zaxxer.hikari.HikariDataSource
    hikari:
      maximum-pool-size: 50
      minimum-idle: 2
```

- 配置 SqlSessionFactory 需要的信息

```yaml
mybatis:
  mapper-locations: classpath*:/mapperxml/*.xml
  type-aliases-package: com.haredot.entity
  configuration:
    call-setters-on-nulls: true
    map-underscore-to-camel-case: true  # 设置驼峰命名和数据库蛇形命名自动转换
```

- 配置 分页插件相关的信息 (该步骤可以省略，因为系统提供的默认配置基本符合要求)

```yaml
# 配置分页插件
pagehelper:
  reasonable: true
```

- 在 配置类上，使用 @MapperScann 扫描 持久层接口

<br/>

# SpringBoot 配置 日志

```yaml
logging:
  level:
    root: info
    com.haredot.mapper: debug
```